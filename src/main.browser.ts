import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { MainModule } from './main.module';

var platform = platformBrowserDynamic();
platform.bootstrapModule(MainModule)
  .then( moduleRef => console.log("Bootstrapped!"));