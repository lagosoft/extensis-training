import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';

import { Employee } from './models';

import * as datajson from './../data-json'; 

@Injectable()
export class FetchService {
	constructor(public http: Http ) {
	 }

	getInitialTodos(): Observable<string[]> {
		return Observable.of([
			'Default Tasks #1',
			'Default Tasks #2',
			'Default Tasks #3',
		]).delay(300);
	}

	getInitialEmployees(): Observable<any> {
		return Observable.from(datajson.data).delay(100);
	}
}