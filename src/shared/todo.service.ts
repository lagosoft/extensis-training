import { Injectable } from '@angular/core';
import { Task, TaskList } from './models';
import { Store } from './store.service';
import { FetchService } from './fetch.service';

@Injectable()
export class FvcTodoService {
	// taskList = [];
	constructor(public fetchService: FetchService,
							public store: Store){
		console.log("Creating todo service.");
	}

	syncWithServer() {
		console.log("Todo Service starting to synch with server.");
		this.fetchService.getInitialTodos()
		.subscribe( data => {
			console.log("Returned from server with state");
			var tasks = data.map((task) => {
				return new Task( Math.random(), task);
			});
			this.store.update('todos', tasks);
		})
	}

	getChanges() {
		return this.store.changes
			.pluck<Task[]>('todos');
	}

	getCurrentTasks() {
		return this.store.get('todos');
	}

	createTodo(task : string) {
		var newTask = new Task(Math.random(), task);
		var currentTasks = this.store.get('todos');
		var newTasks = [...currentTasks, newTask];
		this.store.update('todos', newTasks);
	}

	completeTask(item:Task) {
		var currentTasks = this.store.get('todos');
		var taskList = currentTasks.filter ( task => task.id !== item.id);
		this.store.update('todos', taskList);
	}
}