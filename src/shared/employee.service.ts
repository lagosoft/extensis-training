import { Injectable } from '@angular/core';

import { FetchService } from './fetch.service';
import { Store } from './store.service';
import { Employee } from './models';

@Injectable()
export class EmployeeService {

	constructor(public fetchService: FetchService, public store: Store) {}

	syncWithServer() {
		console.log("Employee Service starting to synch with server.");
		this.fetchService.getInitialEmployees()
		.subscribe( data => {
				this.createEmployee(data);
			});
	}

	getChanges() {
		return this.store.changes.pluck<any[]>('employees');
	}

	createEmployee(employee : Employee) {
		var currentEmployees = this.store.get('employees');
		employee.index = currentEmployees.length;
		employee['_id'] = Math.random().toString();
		var newEmployees = [...currentEmployees, employee];
		this.store.update('employees', newEmployees);
	}

	fireEmployee(employee: Employee) {
		var currentEmployees = this.store.get('employees');
		var employeeList = currentEmployees.filter ( employeeItem => employeeItem._id !== employee._id);
		this.store.update('employees', employeeList);
	}
}