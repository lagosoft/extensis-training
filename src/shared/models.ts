/*-------------------  'todo' feature -------------------*/
export class Task {
	id: number;
	task: string;

	constructor( id: number, task: string) {
		this.id = id;
		this.task = task;
	}

	getShort() {
		return this.id.toString().substr(2,4);
	}
}

export class TaskList {
	tasks: Task[];

	constructor( taskArr: Task[]) {
		this.tasks = taskArr;
	}
}

/*------------------- For the 'company-employees' feature -------------------*/ 
export class Employee {
	index: number;
	_id: string;
	name: string;
	gender: string;

	constructor( name: string, gender: string, index: number) {
		this.name = name;
		this.gender = gender;
		this.index = index;
	}
}

export class EmployeeList {
	employees: Employee[];
	
	constructor( employeeList: Employee[]) {
		this.employees = employeeList;
	}
}