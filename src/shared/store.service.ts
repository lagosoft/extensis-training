import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import * as datajson from './../data-json';

import { Task, Employee } from './models';

export interface AppState{
		todos: Task[];
		employees: Employee[];
}

var _defaultValue: AppState = {
	todos: [],
	employees: [],
};

var _store = new BehaviorSubject(_defaultValue);

 @Injectable()
export class Store {
	changes = _store
		.asObservable()
		.do(value => {
			console.log("_store changed");
		});
		
	setCurrentState( state: any ) {
		_store.next(state);
	}

	getCurrentState() {
		return _store.value;
	}

	get(property) {
		return _store.value[property];
	}
	
	update(property, value) {
		var currentState = _store.value;
		currentState[property] = value;
		_store.next(currentState);
	}
}