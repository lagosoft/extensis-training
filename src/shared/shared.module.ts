import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { Employee } from './models';

import { FvcTodoService } from './todo.service';
import { EmployeeService } from './employee.service';
import { FetchService } from './fetch.service';
import { Store } from './store.service';

var providers = [
	FvcTodoService,
	FetchService,
	EmployeeService,
	Store
];

var modules = [
	CommonModule,
	HttpModule,
	RouterModule,
	ReactiveFormsModule,
	FormsModule
];

var sharedComponents = [
	Employee
];

@NgModule({
	declarations: [],
	imports: [...modules],
	exports: [...modules]
})
export class SharedModule { 
	static withProviders(): ModuleWithProviders {
		return {
			ngModule: SharedModule,
			providers
		}
	}
}
