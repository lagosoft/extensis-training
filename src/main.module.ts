import './polyfills.browser';
import { BrowserModule } from '@angular/platform-browser';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GetEmployees, routes } from './app/routes';

import { AppModule } from './app/app.module';
import { AppComponent } from './app/app.component';
import { SharedModule } from './shared/shared.module';
import { FvcTodoService } from './shared/todo.service';
import { EmployeeService } from './shared/employee.service';

@NgModule({
  bootstrap: [
    AppComponent
  ],
  providers: [ GetEmployees ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    AppModule,
    SharedModule.withProviders()
  ]
})
export class MainModule{

  constructor(private employeeService: EmployeeService,
              private todoService: FvcTodoService){
    console.log('main Module created');
    todoService.syncWithServer();
    employeeService.syncWithServer();
  } 
}