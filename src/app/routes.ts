import { Injectable } from '@angular/core';
import { Route, Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs'; 

import { Store } from '../shared/store.service';

import { HomeComponent } from './home/home.component';
import { FvcTodoComponent } from './todo/todo.component';

import { CompanyComponent } from './company/company.component';
import { EmployeeComponent } from './company/employee/employee.component';
import { NewEmployeeComponent } from './company/employee/new-employee.component';


@Injectable()
export class GetEmployees implements Resolve<GetEmployees> {
  constructor( public store: Store) {}

  resolve( route: ActivatedRouteSnapshot) {
    console.log(" ROUTE PARAMS : " , route.params);
    var id = route.params['_id'];
    var filter = this.store.get('employees')
      .filter( employee => {
        return employee._id === id;
      })[0];

      return Observable.of(filter);
  } 
} 

export var routes: Route[] = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'todos',
    component: FvcTodoComponent
  },
  {
    path: 'employees',
    component: CompanyComponent
  },
  {
    path: 'employees/add',
    component: NewEmployeeComponent
  },
  {
    path: 'employees/:_id',
    component: CompanyComponent,
    resolve: [ GetEmployees ],
    children: [
      {
        path: '',
        component: EmployeeComponent
      }
    ]
  }
];