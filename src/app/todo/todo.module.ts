import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';

import { FvcTaskComponent } from './task/task.component';
import { FvcTodoComponent } from './todo.component';

var fvcTodoComponents = [
  FvcTodoComponent,
  FvcTaskComponent
];

@NgModule({
	imports: [
		CommonModule,
		HttpModule
	],
	declarations: [
		...fvcTodoComponents
	],
	exports: [
		...fvcTodoComponents
	]
})
export class FvcTodoModule { }
