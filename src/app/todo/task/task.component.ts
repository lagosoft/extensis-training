import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
	selector: 'fvc-task',
	template: `
	<div *ngIf="task">
	{{ task.task }}
	</div>
	<button (click)='completeTask()'>Complete</button>
	`
})
export class FvcTaskComponent {
	@Input()
	task: any;

	@Output()
	completed = new EventEmitter();

	constructor() { 
		console.log("I am constructor: " + this.task);
	}

	ngOnInit() {
		console.log("I am ngOnInit data is bound: " + this.task);
	}

	completeTask() {
		console.log("Completing task: ", this.task);
		this.completed.emit(this.task);
	}
}