import { Component, OnInit } from '@angular/core';

import { Task, TaskList } from './../../shared/models';
import { FvcTodoService } from './../../shared/todo.service'
@Component({
	selector: 'fvc-todo',
	templateUrl: `
		<H1>FVC TODO</H1>
		<input
			(input)='inputDetector($event.target.value)'
			[value]='task' 
			type='text' 
			placeholder='input a task'
		>
		<button [disabled]="task.length===0" (click)='createTodo(task)'>Add Todo</button>
		<ul>
			<li *ngFor='let item of taskList; let i = index'>
				<fvc-task 
					[task]="item"
					(completed)="completeTask($event)"
				></fvc-task>
			</li>
		</ul>
	`
})
export class FvcTodoComponent {
	task: string='';
	taskList: Task[] = [];
	subscription;

	constructor(public todoService: FvcTodoService) {
		this.todoService.getChanges()
			.subscribe( data => {
				this.taskList = data;
			});
	}

	inputDetector(value: string) {
		this.task = value;
	}

	clearTaskInput(){
		this.task = '';
	}

	createTodo(task: string) {
		this.todoService.createTodo(task);
		this.clearTaskInput();
	}

	completeTask(item: any){
		this.todoService.completeTask(item);
	}
}