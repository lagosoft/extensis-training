import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FvcTodoModule } from './todo/todo.module';
import { CompanyModule } from './company/company.module';
import { SharedModule } from './../shared/shared.module';

import { HomeComponent } from './home/home.component';

@NgModule({
  imports: [
    FvcTodoModule,
		CompanyModule,
    SharedModule
  ],
  declarations: [
    AppComponent,
    HomeComponent
  ],
  exports: [
		FvcTodoModule,
		CompanyModule
	]
})
export class AppModule {} 