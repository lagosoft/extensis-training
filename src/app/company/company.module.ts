import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { SharedModule } from '../../shared/shared.module';
import { CompanyComponent }   from './company.component';
import { NewEmployeeComponent } from './employee/new-employee.component';
import { EmployeeComponent } from './employee/employee.component';

var companyComponents = [
	CompanyComponent,
	NewEmployeeComponent,
	EmployeeComponent
];

@NgModule({
	imports: [
		CommonModule,
		SharedModule
	],
	exports: [
		...companyComponents
	],
	declarations: [
		...companyComponents
	]
})
export class CompanyModule { }
