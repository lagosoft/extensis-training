import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SharedModule } from './../../../shared/shared.module';
import { Employee } from './../../../shared/models';

@Component({
	selector: 'employee',
	template: `
		<div align='center'>
			<hr/>
			<h3> employee details </h3>
			<table>
				<tr>
					<th align='left'>name</th> <td>{{employee.name}}</td>
				</tr>
				<tr>
					<th align='left'>gender</th> <td>{{employee.gender}}</td>
				</tr>
				<tr>
					<th align='left'>employee # </th> <td>{{employee._id}}</td>
				</tr>
			</table>
			<p></p>
			<button (click)="router.navigateByUrl('employees')">close</button>
		</div>
	`
})
export class EmployeeComponent {
	employee: Employee;
	constructor(public router: Router, public route: ActivatedRoute) {
		this.route.data
			.map(data => data[0])
			.subscribe( employee => {
				this.employee = employee;
			});
	}
}