import { Component, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup, Validators, FormControlName } from '@angular/forms';

import { EmployeeService } from '../../../shared/employee.service';
import { Employee } from '../../../shared/models';

@Component({
	selector: 'add-employee',
	styleUrls: ['employee.css'],
	template: `
		<hr/>
		<H5>Add an Employee:</H5>
		<br/>
		{{ newEmployee.name }}  <strong *ngIf="newEmployee.name">{{ newEmployee?.gender }}</strong>
		
		<form [formGroup]="form">
			<span>name:</span>
			<input type="text"
						 formControlName="name" 
						 placeholder="e.g. john smith"
						 [(ngModel)]="newEmployee.name"
			>
			<br/>
			<span>gender:</span>
			<label class="switch">
				<input
				  [checked]="isEmployeeFemale" type="checkbox"
					formControlName="gender" 
					[(ngModel)]="isEmployeeFemale" (change)="setGender()">
				<div 
					[ngClass]="'slider round ' + (newEmployee.gender.length>0 && !isEmployeeFemale ? 'male' : 'pristine')">
				</div>
			</label>
			<span *ngIf="isEmployeeFemale && newEmployee.gender">female</span>
			<span *ngIf="!isEmployeeFemale && newEmployee.gender">male</span>
			<br/>
			<button [disabled]="!form.valid" (click)="addEmployee(); clearValues(); form.reset();">add employee</button>
			<button [disabled]="!form.dirty || form.pristine" (click)="clearValues(); form.reset();">reset</button>
		</form>
	`
})
export class NewEmployeeComponent {

	form: FormGroup;

	newEmployee:any = {}
	employeeGender = '';
	employeeName = '';
	isEmployeeFemale = false;

	employee: Employee;

	constructor(public employeeService: EmployeeService,
						  public route: ActivatedRoute){
		this.clearValues();

		this.form = new FormGroup({
      name: new FormControl('', Validators.required),
      gender: new FormControl('')
    });
		
		this.form.valueChanges
			.throttleTime(100)
			.subscribe(data => {
				// console.log(data['name']);
		});

		this.route.data
			.map(data => data[0])
			.subscribe( employee => {
				this.employee = employee;
			});
	}

	funTimes(){
		console.log("FUN TIMES");
	}

	clearValues(){
		this.newEmployee = {
			name: '',
			gender: ''
		};
		this.employeeGender = '';
		this.employeeName = '';
	}

	addEmployee(){
		if(this.newEmployee.name.length>0){
			console.log(" Adding");
			this.employeeService.createEmployee(this.newEmployee);
			this.clearValues();
		}
	}

	setName(name) {
		this.newEmployee.name = name;
		console.log(this.newEmployee.name);
	}

	setGender() {
		this.newEmployee.gender = (this.isEmployeeFemale ? 'female' : 'male');
	}
}