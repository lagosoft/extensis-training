import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { EmployeeService } from './../../shared/employee.service';

@Component({
	selector: 'company',
	template: `
		<H1>Companies</H1>
		<button (click)="showAll()">All Employees</button>
		<p></p>
		<button (click)="showOnlyFemale();router.navigateByUrl('employees')">Female employees</button>
		<button (click)="router.navigateByUrl('employees');showOnlyMale();">Male employees</button>

		<ul>
			<li *ngFor="let employee of filteredEmployees; let i = index">
				<small>{{employee._id}} -</small><strong>{{ employee.name }} </strong> {{ employee.gender }}
				<button *ngIf="isViewingAll" 
					(click)="router.navigateByUrl('employees/'+ employee._id)"
				>
				view!</button>
				<button *ngIf="isViewingAll" (click)="removeEmployee(employee);showAll();router.navigateByUrl('employees')">Fire!</button>
			</li>
		</ul>
		<router-outlet></router-outlet>
	`
})
export class CompanyComponent{
	employeeArray = [];
	filteredEmployees = [];
	isViewingAll = true;
	
	constructor(public employeeService: EmployeeService,
							public router: Router) {

		this.employeeService.getChanges()
			.subscribe( data => {
				this.employeeArray = data;
			});
		this.showAll();
	}

	addEmployee(newEmployee){
		this.employeeService.createEmployee(newEmployee);
	}

	viewEmployeeByIndex(_id) {
		this.router.navigateByUrl('employees/employee/' + _id);
	}

	removeEmployee(employee){
		this.employeeService.fireEmployee(employee);
	}

	showAll() {
		this.isViewingAll = true;
		this.filteredEmployees = this.employeeArray;
	}

	showOnlyFemale(){
		this.isViewingAll = false;
		this.filteredEmployees = this.employeeArray.filter(person => person.gender === 'female');
		console.log(this.filteredEmployees);
	}

	showOnlyMale(){
		this.isViewingAll = false;
		this.filteredEmployees = this.employeeArray.filter(person => person.gender === 'male');
		console.log(this.filteredEmployees);
	}
}