import { Component } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app',
  template: `
    <div>
      Routes :
      <button (click)="router.navigateByUrl('home')">home</button>
      <button (click)="router.navigateByUrl('todos')">todos</button>
      <button (click)="router.navigateByUrl('employees')">employees</button>
      <button (click)="router.navigateByUrl('employees/add')">new hire</button>
    </div> 
    <router-outlet></router-outlet>
  `
})
export class AppComponent{
  constructor(public router : Router){}
}